import React, {Component} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import api from "../../api/api";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import {noProfilePicture} from "../../consts";

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }

    componentWillMount() {
        api.getSampleData().then(items => {
            this.setState({items})
        })
    }

    openDetails = () => {
        api.getDetails().then(details => {
            this.props.navigation.navigate('Details', {data: details});
        })
    }

    renderList = () => {
        return this.state.items.map((item, index) => {
            return (
                <Thumbnail
                    key={index}
                    data={item}
                    openDetails={this.openDetails}
                />
            )
        })
    }

    render() {
        console.log(this.state)
        return (
            <View style={styles.background}>
                <ScrollView style={styles.itemContainer}>
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Profile')}}
                    >
                        <Image
                            source={{uri: noProfilePicture}}
                            style={styles.profilePicture}
                        />
                    </TouchableOpacity>
                    {this.renderList()}
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#555',
        paddingHorizontal: '5%',
    },
    profilePicture: {
        height: 25,
        width: 25,
        borderRadius: 25/2,
        alignSelf: 'flex-end',
    },
    itemContainer: {
        width: '100%',
        marginTop: '5%',
    }
}