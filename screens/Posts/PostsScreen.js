import React, {Component} from 'react';
import {View, Text} from 'react-native';
import PropTypes from 'prop-types';

export default class PostsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>PostsScreen Component</Text>
            </View>
        )
    }
}