import React, {Component} from 'react';
import {View, Text, ScrollView, Image, Dimensions, ImageBackground, TouchableOpacity, Linking} from 'react-native';
import PropTypes from 'prop-types';
import Icons from "react-native-vector-icons/SimpleLineIcons";
import IconsFA from "react-native-vector-icons/FontAwesome";
import {getDay} from "../../utils/utils";
import {googleMapsURL} from "../../consts";
import call from 'react-native-phone-call'

const windowHeight = Dimensions.get('window').height;

const BackButton = (props) => (
    <TouchableOpacity onPress={() => {
        props.navigation.goBack()
    }}>
        <Icons name="arrow-left" size={24} color={'white'}/>
    </TouchableOpacity>
)

export default class DetailsScreen extends Component {
    static getDerivedStateFromProps(props) {
        return {
            data: props.navigation.state.params.data
        }
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggleFavourite = () => {
        (this.state.isFavourited === 1)
            ? this.setState({isFavourited: 0})
            : this.setState({isFavourited: 1})
    }

    renderFavourited() {
        return (this.state.isFavourited === 1)
            ? <IconsFA name={'heart'} size={24} color={'red'}/>
            : <IconsFA name={'heart-o'} size={24} color={'white'}/>
    }

    renderOpeningTimes(days) {
        console.log('days', days)
        return days.map(day => {
            return (
            <View key={day.day} style={styles.openHoursDay}>
                <Text style={{color: '#333', fontWeight: 'bold'}}>{day.day}</Text>
                <Text>{`${day.open} - ${day.close}`}</Text>
            </View>
            )
        }

        )
    }

    render() {
        let {data} = this.state;
        console.log(data)
        return (
            <ScrollView style={{flex: 1}}>
                <ImageBackground
                    source={{uri: data.image}}
                    style={{width: '100%', height: windowHeight * 0.35}}
                >
                    <View style={styles.buttons}>
                        <View style={{position: 'absolute', top: 10, right: 10}}>
                            <TouchableOpacity onPress={() => {
                                this.toggleFavourite()
                            }}>
                                {this.renderFavourited()}
                            </TouchableOpacity>
                        </View>
                        <View style={{position: 'absolute', top: 10, left: 10}}>
                            <BackButton navigation={this.props.navigation}/>
                        </View>
                    </View>
                </ImageBackground>
                <View style={styles.information}>
                    <Text style={styles.title}>{data.title}</Text>
                    <Text style={styles.subtitle}>{`${getDay()} ${data.today}`}</Text>
                </View>
                <View style={styles.links}>
                    <TouchableOpacity style={styles.link} onPress={() => {
                        Linking.openURL(data.website)
                    }}>
                        <Icons name="screen-desktop" size={24} color={'#333'}/>
                        <Text>Visit the website</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.link} onPress={() => {
                        Linking.openURL(`${googleMapsURL}${data.location}`)
                    }}>
                        <Icons name="location-pin" size={24} color={'#333'}/>
                        <Text>Get directions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.link} onPress={() => {
                        call({number: data.phone.toString(), prompt: true})
                    }}>
                        <Icons name="phone" size={24} color={'#333'}/>
                        <Text>Call the venue</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.information}>
                    <Text style={styles.title}>Opening Times</Text>
                    <ScrollView horizontal={true} style={styles.openHoursContainer}>
                        <View style={styles.openHours}>
                            {this.renderOpeningTimes(data.open_hours)}
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.information}>
                    <Text style={styles.title}>Details</Text>
                    <Text>
                        {data.description}
                    </Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = {
    buttons: {
        marginTop: 10
    },
    information: {
        paddingLeft: 10,
        paddingTop: 25
    },

    title: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 14,
        marginTop: 5
    },
    links: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 30,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        borderTopColor: 'black',
        borderTopWidth: 1,
        paddingVertical: 20
    },
    link: {
        alignItems: 'center'
    },
    openHoursContainer: {
        marginTop: 10,
    },
    openHours: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'

    },
    openHoursDay: {
        marginRight: 30
    }

}