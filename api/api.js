import axios from 'axios';
import * as consts from '../consts';

const api = {
    getSampleData() {
        return axios.get(consts.listURL)
            .then(list => list.data.data)
            .catch(err => {return {err: `Error: ${err}`}})
    },

    getDetails() {
        return axios.get(consts.detailsURL)
            .then(list => list.data.data)
            .catch(err => {return {err: `Error: ${err}`}})
    }
};

export default api;