# ModelVillage Coding Test

## Introduction

> Coding test for Model Village

## Installation

### yarn

```console
$ git clone https://gitlab.com/filipevalente/ModelVillage
$ cd ModelVillage/
$ yarn && yarn start
$ react-native run-android
```
> Note: react-native run-android will start the packager if wasn't started before. Be sure that you also have the android emulator up and running. If you prefer to run the iOS version, do react-native run-ios instead of react-native run android (you'll need MacOS). If that doesn't work, open the project using xcode and build from there.

### npm

```console
$ git clone https://gitlab.com/filipevalente/ModelVillage
$ cd ModelVillage/
$ npm install && npm start
$ react-native run-android
```