/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, StatusBar,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import HomeScreen from "./screens/Home/HomeScreen";
import PostsScreen from "./screens/Posts/PostsScreen";
import FavouritesScreen from "./screens/Favourites/FavouritesScreen";
import RedeemedScreen from "./screens/Redeemed/RedeemedScreen";
import ProfileScreen from "./screens/Profile/ProfileScreen";
import Icons from "react-native-vector-icons/SimpleLineIcons";
import DetailsScreen from "./screens/Details/DetailsScreen";

type Props = {};

const HomeStack = createStackNavigator({
    Home: HomeScreen,
    Details: DetailsScreen,
}, {
    headerMode: 'none',

});


let AppNavigator = createBottomTabNavigator({
    Home: HomeStack,
    Posts: PostsScreen,
    Favourites: FavouritesScreen,
    Redeemed: RedeemedScreen,
    Profile: ProfileScreen


}, {
    navigationOptions: ({navigation}) => ({
        tabBarIcon: ({focused, tintColor}) => {
            const {routeName} = navigation.state;
            let iconName;
            switch (routeName) {
                case 'Home':
                    iconName = 'home';
                    break;
                case 'Posts':
                    iconName = 'camera';
                    break;
                case 'Favourites':
                    iconName = 'heart';
                    break;
                case 'Redeemed':
                    iconName = 'present';
                    break;
                case 'Profile':
                    iconName = 'user';
                    break;
            }

            // You can return any component that you like here! We usually use an
            // icon component from react-native-vector-icons
            return <Icons name={iconName} size={20} color={tintColor}/>;
        },
    }),
    tabBarOptions: {
        style: {
            backgroundColor: '#222'
        },
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
    },
});

export default class App extends Component {
    render() {
        return (<View style={{flex: 1}}>
            <StatusBar hidden={true} />
            <AppNavigator />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
