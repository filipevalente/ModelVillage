import React, {Component} from 'react';
import {View, Text, Dimensions, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Icons from "react-native-vector-icons/SimpleLineIcons";
import IconsFA from "react-native-vector-icons/FontAwesome";

const windowHeight = Dimensions.get('window').height;

export default class Thumbnail extends Component {
    static getDerivedStateFromProps(props) {
        return {
            isFavourited: props.data.is_favourited
        }
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    renderDistance() {
        return (this.props.data.offer)
            ? <View style={styles.distance}>
                <Icons name={'location-pin'} size={22} color={'#333'}/>
                <Text style={{color: '#333'}}> {this.props.data.offer.location.walking_time}</Text>
            </View>
            : <View></View>
    }

    toggleFavourite = () => {
        (this.state.isFavourited === 1)
            ? this.setState({isFavourited: 0})
            : this.setState({isFavourited: 1})
    }

    renderFavourited() {
        return (this.state.isFavourited === 1)
            ? <IconsFA name={'heart'} size={24} color={'red'}/>
            : <IconsFA name={'heart-o'} size={24} color={'white'}/>
    }

    openDetails = () => {

    }

    render() {
        return (
            <TouchableOpacity style={styles.thumbnail} onPress={this.props.openDetails}>
                    <Image
                        source={{uri: this.props.data.image}}
                        style={{height: '50%'}}
                    />
                    <View style={{position: 'absolute', top: 10, right: 10}}>
                        <TouchableOpacity onPress={() => {this.toggleFavourite()}}>
                            {this.renderFavourited()}
                        </TouchableOpacity>
                    </View>
                    <View style={styles.information}>
                        <Text style={styles.title}>{this.props.data.title}</Text>
                        <Text style={styles.subtitle}>{this.props.data.subtitle}</Text>
                        {this.renderDistance()}
                    </View>
            </TouchableOpacity>
        )
    }
}

const styles = {
    thumbnail: {
        height: windowHeight*0.5,
        marginTop: 15,
        marginBottom: 15,
        borderRadius: 10,
        backgroundColor: 'white',
        overflow: 'hidden',
        position: 'relative'
    },
    information: {
        paddingLeft: 10,
        paddingTop: 25
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 14,
        marginTop: 5
    },
    distance: {
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    }
}